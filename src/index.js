import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './shared/registerServiceWorker';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './shared/store';
import Layout from './layout';

import Home from './routes/home/show';
import Search from './routes/search/show';
import Model from './routes/make/model/show';

ReactDOM.render(
  <Provider store={ store }>
    <Router>
      <Layout>
        <Switch>
          <Route path='/' component={ Home } exact />
          <Route path='/home' component={ Home } exact />
          <Route path='/search' component={ Search }></Route>
          <Route path='/make/model/:id' component={ Model }></Route>
          <Route render={ () => { return (<h1>Not Found</h1>); } } />
        </Switch>
      </Layout>
    </Router>
  </Provider>
  , document.getElementById('root'));
registerServiceWorker();
