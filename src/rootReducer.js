import { combineReducers } from 'redux';
import { appState } from './shared/appReducer';
import { carOfTheWeekState } from './routes/home/carOfTheWeek/reducer';
import { carMakeState } from './routes/search/searchByMakeAndModel/reducer';
import { carModelState } from './routes/make/model/reducer';

const rootReducer = combineReducers({
  appState,
  carOfTheWeekState,
  carMakeState,
  carModelState,
})

export default rootReducer;