import React from 'react';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import LinearProgress from '@material-ui/core/LinearProgress';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { APP_IN_FETCHING } from '../../../shared/appReducer';
import { CAR_MODEL_REQUESTED } from './reducer';
import styled from 'styled-components';

class Model extends React.Component {
  componentWillMount () {
    if (this.props.carModelState.length === 0) {
      this.props.dispatch({ type: APP_IN_FETCHING });
      this.props.dispatch({ type: CAR_MODEL_REQUESTED });
    }
  }

  render() {
    const ModelImage = styled.img`
      width: 100%
    `;
    const { isFetching } = this.props.appState ? this.props.appState : false;
    const Loader = styled(LinearProgress)`
      height: 2px;
      transition: 1s all ease;
      opacity: ${isFetching ? '1' : '0'};
      height: ${isFetching ? '2px' : '0'};
    `;

    console.log('Model - render - ', this.props.match.params);
    console.log('Model - render - ', this.props.carModelState);
    return (
      <div>
        <Loader />
        { this.props.carModelState.map(item => (
          this.props.match && this.props.match.params && this.props.match.params.id.indexOf(item.id) > -1 &&
          <Card key={ item.id }>
            <CardHeader title={ item.name } />
            <ModelImage src={ item.imageUrl } />
            <CardContent>
              <Typography component="p">${ item.price }</Typography>
            </CardContent>
          </Card>
        ))}
      </div>
    );
  }
}

function mapStateToProps ({ appState, carModelState }) {
  console.log('SearchByMakeAndModel - mapStateToProps - carModelState', carModelState);
  return { appState, carModelState };
}

export default connect(
  mapStateToProps
)(Model);

