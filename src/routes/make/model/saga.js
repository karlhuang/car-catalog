import { call, put, takeLatest } from 'redux-saga/effects';
import {
  CAR_MODEL_REQUESTED,
  CAR_MODEL_RECEIVED,
  CAR_MODEL_REJECTED
} from './reducer';
import { APP_IN_FETCHED } from '../../../shared/appReducer';
import { fetchJSON } from '../../../shared/helper';

async function fetchCarModel () {
  const res = await fetchJSON(process.env.REACT_APP_CAR_MODEL_URL);
  return res;
}

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* carModelWorker(action) {
  const response = yield call(fetchCarModel);
  const type = response ? CAR_MODEL_RECEIVED : CAR_MODEL_REJECTED;
  yield put({ type: APP_IN_FETCHED });
  yield put({ type, response });
}

function* carModelSaga() {
  yield takeLatest(CAR_MODEL_REQUESTED, carModelWorker);
}

export default carModelSaga;
