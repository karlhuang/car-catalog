export const CAR_MODEL_REQUESTED = 'CAR_MODEL_REQUESTED';
export const CAR_MODEL_RECEIVED = 'CAR_MODEL_RECEIVED';
export const CAR_MODEL_REJECTED = 'CAR_MODEL_REJECTED';

export function carModelState(state = [], action) {
  switch (action.type) {
    case CAR_MODEL_REQUESTED:
      return state;
    case CAR_MODEL_RECEIVED:
      return action.response;
    case CAR_MODEL_REJECTED:
      return state;
    default: {
      return state;
    }
  }
}
