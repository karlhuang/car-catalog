import { call, put, takeLatest } from 'redux-saga/effects';
import {
  CAR_MAKE_REQUESTED,
  CAR_MAKE_RECEIVED,
  CAR_MAKE_REJECTED,
} from './reducer';
import { fetchJSON } from '../../../shared/helper';

async function fetchCarMake () {
  const res = await fetchJSON(process.env.REACT_APP_CAR_MAKE_URL);
  return res;
}

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* carMakeWorker(action) {
  const response = yield call(fetchCarMake);
  const type = response ? CAR_MAKE_RECEIVED : CAR_MAKE_REJECTED;
  yield put({ type, response });
}

function* carMakeSaga() {
  yield takeLatest(CAR_MAKE_REQUESTED, carMakeWorker);
}

export default carMakeSaga;