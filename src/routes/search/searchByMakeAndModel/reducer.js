export const CAR_MAKE_REQUESTED = 'CAR_MAKE_REQUESTED';
export const CAR_MAKE_RECEIVED = 'CAR_MAKE_RECEIVED';
export const CAR_MAKE_REJECTED = 'CAR_MAKE_REJECTED';

export function carMakeState(state = [], action) {
  switch (action.type) {
    case CAR_MAKE_REQUESTED:
      return state;
    case CAR_MAKE_RECEIVED:
      return action.response;
    case CAR_MAKE_REJECTED:
      return state;
    default: {
      return state;
    }
  }
}
