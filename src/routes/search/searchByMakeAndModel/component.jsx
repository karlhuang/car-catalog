import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from 'react-router-dom';
import { CAR_MAKE_REQUESTED } from './reducer';
import { CAR_MODEL_REQUESTED } from '../../make/model/reducer';

class SearchByMakeAndModel extends React.Component {
  state = {
    checkedMakeId: [],
    checkedModelId: [],
    toggleMakePanel: false,
    showLoaderForMakePanel: false,
    showLoaderForModelPanel: false,
  };

  handleToggleForMakePanel = makeId => () => {
    console.log('SearchByMakeAndModel - handleToggleForMakePanel - this.state.showLoaderForModelPanel', this.state.showLoaderForModelPanel);
    if (this.props.carModelState.length === 0 && !this.state.showLoaderForModelPanel) {
      this.props.dispatch({ type: CAR_MODEL_REQUESTED });
      this.setState(this.startFetchingForModelPanel);
    }
    const { checkedMakeId } = this.state;
    const currentIndex = checkedMakeId.indexOf(makeId);
    const newChecked = [...checkedMakeId];

    if (currentIndex === -1) {
      newChecked.push(makeId);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checkedMakeId: newChecked,
    });
  };

  handleToggleForModelPanel = modelId => () => {
    console.log('SearchByMakeAndModel - handleToggleForModelPanel - this.state.showLoaderForModelPanel', this.state.showLoaderForModelPanel);
    const { checkedModelId } = this.state;
    const currentIndex = checkedModelId.indexOf(modelId);
    const newChecked = [...checkedModelId];

    if (currentIndex === -1) {
      newChecked.push(modelId);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checkedModelId: newChecked,
    });
  };

  handleSelect = () => {
    console.log('handleSelect');
    if (this.props.carMakeState.length === 0 && !this.state.showLoaderForMakePanel) {
      this.props.dispatch({ type: CAR_MAKE_REQUESTED });
      this.setState(this.startFetchingForMakePanel);
    } 
    this.setState({
      toggleMakePanel: !this.state.toggleMakePanel,
    })
  }

  startFetchingForMakePanel = () => { return { showLoaderForMakePanel: true } }

  startFetchingForModelPanel = () => { return { showLoaderForModelPanel: true } }

  render() {
    console.log('SearchByMakeAndModel - render - this.state.showLoaderForMakePanel', this.state.showLoaderForMakePanel);
    console.log('SearchByMakeAndModel - render - this.state.showLoaderForModelPanel', this.state.showLoaderForModelPanel);
    console.log('SearchByMakeAndModel - render - this.state.checkedMakeId', this.state.checkedMakeId);
    console.log('SearchByMakeAndModel - render - this.state.checkedModelId', this.state.checkedModelId);
    
    const LoaderForMakePanel = styled(LinearProgress)`
      height: 2px;
      transition: 1s all ease;
      opacity: ${this.props.carMakeState.length === 0 && this.state.showLoaderForMakePanel ? '1' : '0'};
      height: ${this.props.carMakeState.length === 0 && this.state.showLoaderForMakePanel ? '2px' : '0'};
    `;

    const LoaderForModelPanel = styled(LinearProgress)`
      height: 2px;
      transition: 1s all ease;
      opacity: ${this.props.carModelState.length === 0 && this.state.showLoaderForModelPanel ? '1' : '0'};
      height: ${this.props.carModelState.length === 0 && this.state.showLoaderForModelPanel ? '2px' : '0'};
    `;
    return (
      <div>
        <ExpansionPanel onChange={this.handleSelect} expanded={ this.props.carMakeState.length > 0 && this.state.toggleMakePanel }>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Select Make</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <List>
              { this.props.carMakeState.map(item => (
                <ListItem
                  key={item.id}
                  button
                  onClick={this.handleToggleForMakePanel(item.id)}
                >
                  <Checkbox
                    checked={this.state.checkedMakeId.indexOf(item.id) !== -1}
                    tabIndex={-1}
                    disableRipple
                  />
                  <ListItemText primary={item.name} />
                </ListItem>
              ))}
            </List>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <LoaderForMakePanel />
        <ExpansionPanel disabled={ this.props.carModelState.length === 0 }>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Select Model</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <List>
              { this.props.carModelState.map(item => (
                this.state.checkedMakeId.indexOf(item.makeId) > -1 &&
                <ListItem
                  key={item.id}
                  button
                  onClick={this.handleToggleForModelPanel(item.id)}
                >
                  <Checkbox
                    checked={this.state.checkedModelId.indexOf(item.id) !== -1}
                    tabIndex={-1}
                    disableRipple
                  />
                  <ListItemText primary={item.name} />
                </ListItem>
              ))}
            </List>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <LoaderForModelPanel />
        <Button
          to={`/make/model/${this.state.checkedModelId}`}
          component={Link}
          disabled={ this.state.checkedModelId.length === 0 }>Go
        </Button>
      </div>
    );
  }
}

function mapStateToProps ({ carMakeState, carModelState }) {
  console.log('SearchByMakeAndModel - mapStateToProps - carMakeState', carMakeState);
  console.log('SearchByMakeAndModel - mapStateToProps - carModelState', carModelState);
  return { carMakeState, carModelState };
}

export default connect(
  mapStateToProps
)(SearchByMakeAndModel);
