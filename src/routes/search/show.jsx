import React from 'react';
import SearchByMakeAndModel from './searchByMakeAndModel/component';

const Search = () => {
  return (
    <div>
      <SearchByMakeAndModel />
    </div>
  );
}
export default Search;