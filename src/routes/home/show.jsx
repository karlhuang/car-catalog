import React from 'react';
import CarOfTheWeek from './carOfTheWeek/component';

const Home = () => {
  return (
    <div>
      <CarOfTheWeek />
    </div>
  );
}
export default Home;