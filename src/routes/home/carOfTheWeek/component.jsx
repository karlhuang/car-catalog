import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import LinearProgress from '@material-ui/core/LinearProgress';
import { APP_IN_FETCHING } from '../../../shared/appReducer';
import { CAR_OF_THE_WEEK_REQUESTED } from './reducer';
import { isObjectEmpty } from '../../../shared/helper';
class CarOfTheWeek extends React.Component {
  componentWillMount () {
    if (isObjectEmpty(this.props.carOfTheWeekState)) {
      this.props.dispatch({ type: APP_IN_FETCHING });
      this.props.dispatch({ type: CAR_OF_THE_WEEK_REQUESTED });
    }
  }

  render() {
    console.log('CarOfTheWeek - render', this.props);
    // const { carOfTheWeekState } = this.props.carOfTheWeekState;
    const { isFetching } = this.props.appState ? this.props.appState : false;
    const Loader = styled(LinearProgress)`
      height: 2px;
      transition: 1s all ease;
      opacity: ${isFetching ? '1' : '0'};
      height: ${isFetching ? '2px' : '0'};
    `;

    return (
      <div>
        <Loader />
        <h2>{ !isObjectEmpty(this.props.carOfTheWeekState) ? this.props.carOfTheWeekState.review : 'Loading...' } </h2>
      </div>
    );
  }
}

CarOfTheWeek.defaultProps = { carOfTheWeekState: {review: 'Loading...'} };

function mapStateToProps ({ appState, carOfTheWeekState }) {
  console.log('home - mapStateToProps - carOfTheWeekState', carOfTheWeekState);
  return { appState, carOfTheWeekState };
}

export default connect(
  mapStateToProps
)(CarOfTheWeek);
