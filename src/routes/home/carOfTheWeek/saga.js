import { call, put, takeLatest } from 'redux-saga/effects';
import {
  CAR_OF_THE_WEEK_REQUESTED,
  CAR_OF_THE_WEEK_RECEIVED,
  CAR_OF_THE_WEEK_REJECTED
} from './reducer.js';
import { APP_IN_FETCHED } from '../../../shared/appReducer';
import { fetchJSON } from '../../../shared/helper';

async function fetchCarOfTheWeek () {
  const res = await fetchJSON(process.env.REACT_APP_CAR_OF_THE_WEEK_URL);
  return res;
}

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* carOfTheWeekWorker(action) {
  const response = yield call(fetchCarOfTheWeek);
  const type = response ? CAR_OF_THE_WEEK_RECEIVED : CAR_OF_THE_WEEK_REJECTED;
  yield put({ type: APP_IN_FETCHED });
  yield put({ type, response });
}

function* carOfTheWeekSaga() {
  yield takeLatest(CAR_OF_THE_WEEK_REQUESTED, carOfTheWeekWorker);
}

export default carOfTheWeekSaga;
