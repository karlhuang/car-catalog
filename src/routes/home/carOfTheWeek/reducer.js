export const CAR_OF_THE_WEEK_REQUESTED = 'CAR_OF_THE_WEEK_REQUESTED';
export const CAR_OF_THE_WEEK_RECEIVED = 'CAR_OF_THE_WEEK_RECEIVED';
export const CAR_OF_THE_WEEK_REJECTED = 'CAR_OF_THE_WEEK_REJECTED';

export function carOfTheWeekState(state = {}, action) {
  switch (action.type) {
    case CAR_OF_THE_WEEK_REQUESTED:
      return state;
    case CAR_OF_THE_WEEK_RECEIVED:
      return { ...action.response };
    case CAR_OF_THE_WEEK_REJECTED:
      return state;
    default: {
      return state;
    }
  }
}
