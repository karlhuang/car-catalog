import { fork } from 'redux-saga/effects';
import carOfTheWeekSaga from './routes/home/carOfTheWeek/saga';
import carMakeSaga from './routes/search/searchByMakeAndModel/saga';
import carModelSaga from './routes/make/model/saga';

const sagas = [
  carOfTheWeekSaga,
  carMakeSaga,
  carModelSaga,
];

export default function* root() {
  yield sagas.map(saga => fork(saga));
}
