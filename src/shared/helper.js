function wait(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export async function fetchJSON(url, fallbackData) {
  try {
    if (url.indexOf('.json') > -1 && process.env.NODE_ENV === 'development') {
      await wait(1000);
    }
    const response = await fetch(url);
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
    return fallbackData;
  }
}

export const isObjectEmpty = obj => Object.keys(obj).length === 0 && obj.constructor === Object;