export const APP_IN_FETCHING = 'APP_IN_FETCHING';
export const APP_IN_FETCHED = 'APP_IN_FETCHED';

const INITIAL_STATE = {
  isFetching: false
};

export function appState(state = INITIAL_STATE, action) {
  switch (action.type) {
    case APP_IN_FETCHING: {
      return { isFetching: true };
    }
    case APP_IN_FETCHED: {
      return { isFetching: false };
    }
    default: {
      return state;
    }
  }
}